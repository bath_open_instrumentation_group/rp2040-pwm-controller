from __future__ import annotations
import logging
import re
import threading
from typing import Optional, Union

import serial

class PWMController:
    """A class to control PWM pins on an Arduino RP2040 Connect
    
    This uses `pyserial` internally, so is suitable for RS232 controlled
    units, or USB-serial.

    Pretty much all the functionality is on a per-channel basis. This is
    accessed by subscripting the object, i.e. pwm[i].
    """
    _ser = None
    _ser_lock = None
    termination = "\n"
    _n_channels = None

    def __init__(self, port: str = "/dev/ttyUSB0", n_channels: int = 16):
        self._ser = serial.Serial(port, 9600, timeout=1)
        self._ser_lock = threading.RLock()
        # Query the device and match its model number
        m = self.query("get_n_channels", "([0-9]+)")
        self._n_channels = int(m.group(1))
        print(f"Connected with {self.n_channels} channels.")

    def ensure_port_is_open(self):
        if not self._ser:
            raise IOError("The serial port is missing!")
        if not self._ser.is_open:
            logging.warning("The serial port was opened implicitly - should do this explicitly")
            self._ser.open()

    def query(self, message: str, response_pattern: Optional[str]=None) -> Union[str, re.Match]:
        """Send a message to the box, and read the reply
        
        `message` is the string to send - it should not be terminated with
        CRLF, that's done by this function.
        
        `response_pattern` should be a regular expression. If provided, we
        will attempt to match the response with the pattern, and return a
        match object (from `re.match(response_pattern, response)`). If the
        match fails, we raise a hopefully-helpful IOError.
        """
        self.ensure_port_is_open()
        with self._ser_lock:
            self._ser.reset_input_buffer()
            self._ser.write((message + self.termination).encode())
            response = self._ser.readline().decode().rstrip(" \r\n")
        if response_pattern is None:
            return response
        else:
            m = re.match(response_pattern, response)
            if m:
                return m
            else:
                raise IOError(
                    "The serial device responded unexpectedly.\n"
                    f"    Sent: '{message}'\n"
                    f"    Received: '{response}'\n"
                    f"    Pattern: '{response_pattern}'"
                )

    @property
    def n_channels(self) -> int:
        """The number of channels this device has"""
        return self._n_channels
    
    def validate_index(self, index: int) -> int:
        """Check a channel index is valid"""
        i = int(index)
        if i < 0:
            raise IndexError("Channel indices must be >= 0")
        if i >= self.n_channels:
            raise IndexError(f"Channel indices must be < {self.n_channels}")
        return i

    def __getitem__(self, index: int):
        """Retrieve the value of a PWM channel"""
        i = self.validate_index(index)
        m = self.query(f"get_channel {i}", "([0-9.]+)")
        return float(m.group(1))
    
    def __setitem__(self, index: int, level: float) -> None:
        """Set the value of a PWM channel"""
        i = self.validate_index(index)
        l = float(level)
        assert l >= 0, ValueError("PWM values must be positive percentages")
        assert l <= 100, ValueError("PWM values must be less than 100")
        self.query(f"set_channel {i} {l}")

