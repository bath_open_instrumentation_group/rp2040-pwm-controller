import rp2040_pwm
import time
import argparse

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--port", default="/dev/ttyACM0")

    args = parser.parse_args()
    pwm = rp2040_pwm.PWMController(port=args.port)

    for i in range(pwm.n_channels):
        print(f"Channel {i}'s value: '{pwm[i]}'")
    print()

    try:
        for i in range(pwm.n_channels):
            print(f"Testing channel {i}")
            for v in (10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 0):
                print(f"  Setting value to: '{v}' ... ", end="")
                pwm[i] = v
                time.sleep(0.25)
                print(f"read as: '{pwm[i]}'")
            print()

    finally:
        print(f"\nSetting all channels LOW...")
        for i in range(pwm.n_channels):
            pwm[i] = 0.0

    print("Done.")
