void setup() {
    Serial.begin(9600); // initialize serial communication at 9600 bits per second
    Serial1.begin(9600); // initialize serial communication at 9600 bits per second
}

void loop() {
    if (Serial1.available() > 0) { // check if there is any incoming data
        char incomingByte = Serial1.read(); // read the incoming byte
        Serial.print(incomingByte); // print a message
    }
    if (Serial.available() > 0) { // check if there is any incoming data
        char incomingByte = Serial.read(); // read the incoming byte
        Serial1.print(incomingByte); // print a message
    }
}
