# RP2040 PWM Controller

This is intended to be a minimal but functional Arduino sketch and Python module that provides a 12-channel USB-Serial PWM controller using an Arduino RP2040 Connect board. In principle we could use an actual hardware serial port instead of USB serial, but I'll use USB for now as it makes life easy.

## Using the Arduino code

You will need to install Arduino IDE (I used 2.2.1) and then use the library manager to install the `MBED_RP2040_PWM` library by Khoi Hoang (I used v1.0.1). You'll also need to install the board definition for the RP2040 Connect, though this was fairly automatic for me. Then, open and upload the sketch in `rp2040-pwm-sketch`. You may wish to change the pins it uses, defined at the top of the sketch.

## Installing the Python code

The easiest way to get it running is to install directly from gitlab:

```
pip install git+https://gitlab.com/bath_open_instrumentation_group/rp2040-pwm-controller.git
```

You can install it in editable mode by cloning the repository, then running `pip install -e .` though you will need to be running a recent version of `pip`, as there's no `setup.py`.

## Using

Once installed, you can import and use the class.  See `tests/test_comms.py` for an example. It's intentionally very simple:

```
import rp2040_pwm

controller = rp2040_pwm.PWMController()  # default port is "/dev/ttyUSB0"

for i in range(controller.n_channels):
    print(f"Channel {i} is currently {controller[i]}")

# Enable the first channel
controller[0] = 100

# NB you may use fractional numbers
controller[0] = 33.3

# Turn it off again
controller[0] = 0

```

I've not yet determined whether it's better to turn channels on and off using `mode` or `current`. My guess would be `current`.

## Limitations

I've only implemented `normal` mode so far: our controller doesn't support triggering and only allows two steps in the strobe profile, so we're unlikely to use either of these.  Merge requests are welcome if you have a different controller/application that makes this useful.

## Adding functionality

If there is functionality that's provided by your controoler and not covered by the methods I've implemented so far (e.g. `strobe` or `trigger` mode), you can always communicate with the controller using `controller.query()`.  With one argument, it will send the string to the device (adding a `CRLF` terminator), and return the response (with the CRLF removed).  This only works for single-line responses, more than that will leave subsequent lines sitting in the input buffer. I think that's only an issue if you read the strobe profile, which I've not tried. `query` can take a second argument, `response_pattern`, which is interpreted as a regular expression.  If it's present, we will try to match the response and return a `re.Match` object, i.e. the output of `re.match`. If the match fails, we raise an `IOError` with helpful details (i.e. the message, response, and pattern). This is used by most of the commands that read values, to make sure any errors that occur get picked up early.

## Serial protocol

From the SDK documentation, the port has:

* Baud rate: `9600`
* Data bits: `8`
* Parity: `None`
* Stop bits: `1`

These are the defaults for `pyserial` anyway, though it's wise to specify the baud rate explicitly.

There is a comment in the SDK about wiring, but whatever is required, it is compatible with our off-the-shelf USB-Serial cable. No null modem is needed.

Messages should always be terminated with CRLF, i.e. `b"\r\n". This means that `serial.Serial.readline()` works out of the box.
