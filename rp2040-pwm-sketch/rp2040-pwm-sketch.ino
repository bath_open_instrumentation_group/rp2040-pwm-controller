#include "MBED_RP2040_PWM.h" // Look for the MBED RP2040 PWM library.

// We'll use this input buffer for SERIAL comms
const int INPUT_BUFFER_LENGTH = 64;
char input_buffer[INPUT_BUFFER_LENGTH];

#define SERIAL Serial1

// NB PWM_Pins uses GPIO numbers, **NOT** Arduino pin numbers!!
uint32_t PWM_Pins[] = {2,3,4,5,6,7,8,9,10,11,12,21};
const int n_channels = sizeof(PWM_Pins) / sizeof(uint32_t);
#define EACH_CHANNEL for (uint8_t i = 0; i < n_channels; i++)

// Must be same frequency for same slice - need to check what this really means!!
float freq = 1000000.0f;

float dutyCycle[n_channels];
mbed::PwmOut* pwm[n_channels];

// This is used to make sure that the PWM level we set is inverted, i.e. high means off.
float invert(float level)
{
  return 100.0 - level;
}

void setup() {
  // put your setup code here, to run once:
  EACH_CHANNEL
  {
    dutyCycle[i] = invert(0.0);
    setPWM(pwm[i], PWM_Pins[i], freq, dutyCycle[i]);
  }
  SERIAL.begin(9600);
  while (! SERIAL )
    delay(1);
  //SERIAL.print("Set up with ");
  //SERIAL.print(n_channels);
  //SERIAL.println(" channels");
}

int read_channel(String command, int index, const int n_channels, int* dest)
{
  // Skip a space, then read an integer from the command.
  // It's checked to be between 0 and n_channels - 1, then assigned
  // to dest.
  // Return value is the index of the preceding space, or -1 on error.
  int preceding_space = command.indexOf(' ',index);
  if(preceding_space <= 0){
    SERIAL.println("Bad command.");
    return -1;
  }
  int channel = command.substring(preceding_space+1).toInt();
  if (channel < 0 || channel >= n_channels)
  {
    SERIAL.println("Channel number must be 0 < n < " + String(n_channels));
    return -1;
  }
  *dest = channel;
  return preceding_space;
}

int read_level(String command, int index, float* dest)
{
  // Skip a space, then read a floating point number from the command.
  // It's checked to be between 0 and n_channels - 1, then assigned
  // to dest.
  // Return value is the index of the preceding space, or -1 on error.
  int preceding_space = command.indexOf(' ',index);
  if(preceding_space <= 0){
    SERIAL.println("Bad command.");
    return -1;
  }
  int level = command.substring(preceding_space+1).toFloat();
  if (level < 0.0 || level > 100.0)
  {
    SERIAL.println("level value must be in % (between 0.0 and 100.0)");
    return -1;
  }
  *dest = level;
  return preceding_space;
}

void loop() {
  // put your main code here, to run repeatedly:
  int received_bytes = SERIAL.readBytesUntil('\n',input_buffer,INPUT_BUFFER_LENGTH-1);
  if(received_bytes > 0){
    input_buffer[received_bytes] = '\0';
    String command = String(input_buffer);
    //Serial.println(command);
    if(command.startsWith("set_channel ")){
      int channel;
      int pos = read_channel(command, 0, n_channels, &channel);
      if (pos < 0) return;
      float level;
      pos = read_level(command, pos + 1, &level);
      if (pos < 0) return;
      dutyCycle[channel] = invert(level);
      setPWM(pwm[channel], PWM_Pins[channel], freq, dutyCycle[channel]);
      SERIAL.println("Set channel "+String(channel)+" to "+String(dutyCycle[channel])+"%");
      return;
    }
    if(command.startsWith("get_channel ")){
      int channel;
      int pos = read_channel(command, 0, n_channels, &channel);
      if (pos < 0) return;
      SERIAL.println(invert(dutyCycle[channel]));
      return;
    }
    if(command.startsWith("get_n_channels")){
      SERIAL.println(n_channels);
      return;
    }
    SERIAL.println("Invalid command");
  }
}

// see https://github.com/khoih-prog/RP2040_PWM/blob/main/examples/PWM_MultiChannel/PWM_MultiChannel.ino
